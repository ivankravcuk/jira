package com.cgi.persistence.mapper;

import org.springframework.jdbc.core.RowMapper;
import com.cgi.persistence.entity.Ticket;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketMapper implements RowMapper<Ticket> {

    /**
     * @return mapped Ticket object
     * @see Ticket
     */
    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {

        return Ticket.builder()
                .idTicket(resultSet.getLong("id_ticket"))
                .idPersonAssigned(resultSet.getLong("id_person_assigned"))
                .idPersonCreator(resultSet.getLong("id_person_creator"))
                .name(resultSet.getString("name"))
                .email(resultSet.getString("email"))
                .creationDatetime(resultSet.getTimestamp("creation_datetime").toLocalDateTime())
                .ticketCloseDatetime(resultSet.getTimestamp("ticket_close_datetime").toLocalDateTime())
                .build();
    }
}