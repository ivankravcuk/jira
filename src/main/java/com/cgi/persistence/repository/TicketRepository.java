package com.cgi.persistence.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.cgi.persistence.entity.Ticket;
import com.cgi.persistence.mapper.TicketMapper;

import java.util.List;

/**
 * Spring repository for tickets
 *
 * @see Ticket
 */
@Repository
public class TicketRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public Ticket getTicketById(Long id) {

        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id_ticket = ?", new Object[]{id}, new TicketMapper());
    }

    public Ticket getTicketByName(String name) {

        return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE name = ?", new Object[]{name}, new TicketMapper());
    }

    public int newTicket(Ticket ticket) {
        return jdbcTemplate.update("INSERT INTO ticket (id_ticket, id_person_assigned, id_person_creator, \"name\",email, creation_datetime, ticket_close_datetime)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?); ", ticket.getIdTicket(), ticket.getIdPersonAssigned(), ticket.getIdPersonCreator(), ticket.getName(), ticket.getEmail(), ticket.getCreationDatetime(), ticket.getTicketCloseDatetime());
    }

    public List<Ticket> getAllTickets() {
        return jdbcTemplate.query("SELECT * FROM ticket", new TicketMapper());
    }
}