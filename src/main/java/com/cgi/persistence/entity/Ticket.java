package com.cgi.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * The main entity of JIRA
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Ticket {

    private Long idTicket;
    private Long idPersonAssigned;
    private Long idPersonCreator;
    private String name;
    private String email;
    private LocalDateTime creationDatetime;
    private LocalDateTime ticketCloseDatetime;

}