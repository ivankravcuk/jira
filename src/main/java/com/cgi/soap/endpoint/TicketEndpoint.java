package com.cgi.soap.endpoint;

import com.cgi.persistence.entity.Ticket;
import com.cgi.service.TicketService;
import com.cgi.ws.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * SOAP endpoint for Tickets
 *
 * @see Ticket
 */
@Endpoint
public class TicketEndpoint {

    private static final String NAMESPACE_URI = "http://cgi.com/ws";

    private TicketService ticketService;

    @Autowired
    public TicketEndpoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * request to find Ticket by its id
     *
     * @return Ticket
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getTicketById(@RequestPayload GetTicketByIdRequest request) {

        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        getTicketByIdResponse.setTicketById(ticketService.getTicketById(request.getId()));
        return getTicketByIdResponse;
    }

    /**
     * request to find Ticket by its name
     *
     * @return Ticket
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByNameRequest")
    @ResponsePayload
    public GetTicketByNameResponse getTicketByName(@RequestPayload GetTicketByNameRequest request) {

        GetTicketByNameResponse getTicketByNameResponse = new GetTicketByNameResponse();
        getTicketByNameResponse.setTicketByName(ticketService.getTicketByName(request.getName()));
        return getTicketByNameResponse;
    }

    //TODO: pagination

    /**
     * request to find all Tickets
     *
     * @return ArrayList of all tickets
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllTicketsRequest")
    @ResponsePayload
    public GetAllTicketsResponse getAllTickets(@RequestPayload GetAllTicketsRequest request) {

        GetAllTicketsResponse getAllTicketsResponse = new GetAllTicketsResponse();
        List<TicketInfo> ticketInfoList = new ArrayList<>();
        List<Ticket> ticketList = ticketService.getAllTickets();
        for (Ticket ticket : ticketList) {
            TicketInfo ticketInfo = new TicketInfo();
            BeanUtils.copyProperties(ticket, ticketInfo);
            ticketInfoList.add(ticketInfo);
            try {
                ticketInfo.setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getCreationDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
                ticketInfo.setTicketCloseDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getTicketCloseDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
            } catch (DatatypeConfigurationException e) {
                e.printStackTrace();
            }
        }
        getAllTicketsResponse.getTicket().addAll(ticketInfoList);

        return getAllTicketsResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addTicketRequest")
    @ResponsePayload
    public AddTicketResponse addTicketResponse(@RequestPayload AddTicketRequest addTicketRequest) {

        AddTicketResponse addTicketResponse = new AddTicketResponse();
        TicketInfo ticketInfo = new TicketInfo();
        Ticket ticket = new Ticket(
                addTicketRequest.getTicket().getIdTicket(),
                addTicketRequest.getTicket().getIdPersonAssigned(),
                addTicketRequest.getTicket().getIdPersonCreator(),
                addTicketRequest.getTicket().getName(),
                addTicketRequest.getTicket().getEmail(),
                addTicketRequest.getTicket().getCreationDatetime().toGregorianCalendar().toZonedDateTime().toLocalDateTime(),
                addTicketRequest.getTicket().getTicketCloseDatetime().toGregorianCalendar().toZonedDateTime().toLocalDateTime()
        );

        Ticket savedTicket = ticketService.addTicket(ticket);

        if (ticket == null) {
            System.out.println("Object is null");
        } else {
            BeanUtils.copyProperties(ticket, ticketInfo);
            System.out.println("Success");
            System.out.print(ticket);
            System.out.println(ticketInfo);
        }
        addTicketResponse.setTicket(ticketInfo);
        return addTicketResponse;
    }
}
