package com.cgi.service;

import com.cgi.persistence.entity.Ticket;
import com.cgi.persistence.repository.TicketRepository;
import com.cgi.ws.TicketInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * implementation of TicketService
 *
 * @see TicketService
 * @see TicketRepository
 */
@Service
public class TicketServiceImpl implements TicketService {

    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public TicketInfo getTicketById(Long id) {

        Ticket ticket = ticketRepository.getTicketById(id);

        return mapTicketToTicketInfo(ticket);
    }

    @Override
    public TicketInfo getTicketByName(String name) {

        Ticket ticket = ticketRepository.getTicketByName(name);

        return mapTicketToTicketInfo(ticket);
    }

    @Override
    public Ticket addTicket(Ticket ticket) {
        if (ticketRepository.newTicket(ticket) == 0) {
            return null;
        } else {
            return ticket;
        }

    }


    @Override
    public List<Ticket> getAllTickets() {

        return new ArrayList<>(ticketRepository.getAllTickets());
    }

    private TicketInfo mapTicketToTicketInfo(Ticket ticket) {
        TicketInfo ticketInfo = new TicketInfo();

        ticketInfo.setIdTicket(ticket.getIdTicket());
        ticketInfo.setIdPersonAssigned(ticket.getIdPersonAssigned());
        ticketInfo.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketInfo.setName(ticket.getName());
        ticketInfo.setEmail(ticket.getEmail());
        try {
            ticketInfo.setCreationDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getCreationDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
            ticketInfo.setTicketCloseDatetime(DatatypeFactory.newInstance().newXMLGregorianCalendar(ticket.getTicketCloseDatetime().format(DateTimeFormatter.ISO_DATE_TIME)));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }

        return ticketInfo;
    }
}
