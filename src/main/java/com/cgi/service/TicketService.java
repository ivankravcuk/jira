package com.cgi.service;

import com.cgi.persistence.entity.Ticket;
import com.cgi.ws.TicketInfo;

import java.util.List;

public interface TicketService {

    TicketInfo getTicketById(Long id);

    TicketInfo getTicketByName(String name);

    Ticket addTicket(Ticket ticket);

    List<Ticket> getAllTickets();

}
